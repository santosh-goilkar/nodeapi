const express = require('express');
const router = express.Router();
const pool = require('../dbconfig');
// Route to check if the server is online
router.get('/', (req, res) => {
        res.render('welcome');
});

router.get('/product', async (req, res) => {
    try {
        const [rows] = await pool.query(`SELECT * FROM ProductMaster;`);
        res.render('index', { records: rows });
    } catch (err) {
        console.error('Error retrieving data from the database:', err);
        res.status(500).send('Error retrieving data from the database');
    }
});

module.exports = router;
