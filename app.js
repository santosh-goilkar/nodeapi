const express = require('express');
const app = express();
const indexRouter = require('./routes/index');

// Set view engine
app.set('view engine', 'ejs');
app.set('views', './views');

// Use routes
app.use('/', indexRouter);

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
