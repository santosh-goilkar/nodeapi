const mysql = require('mysql2/promise');

const pool = mysql.createPool({
    host: process.env.DB_HOST || 'mysql',
    user: process.env.DB_USER || 'root',
    password:process.env.DB_PASSWORD || 'password',
    database: process.env.DB_DATABASE || 'mydatabase'
});

module.exports = pool;
