# NodeAPI
Node API to read data from mysql database and display on UI using node.js

## Getting started
I have used Azure Kubernetes services for this assignment.
I have created Kubernetes cluster with name KubCluster in azure portal.

This repo has code for connecting mysql database and reading data from database and display data on UI.

This repo also has YAML files under YAML folder for creating mysql database and node API services from docker image and deploy  in Azure Kubernetes cluster as services with auto scaling using HPA and docker images.

## List of Code Repo and Docker Image urls 

1. Link for the code repository. 
    https://gitlab.com/santosh-goilkar/nodeapi
1. Docker hub URL for docker image 
    https://hub.docker.com/r/santoshgoilkar/nodeapi
1. URL for Service API tier to view the records from backend tier 
    http://172.210.90.145/product

**Note - Above Service API url may be not accessible if Azure Kubernetes cluster service delete/shutdown from portal due azure storage free credit limit crossed.** 

##  Screen recording url [Access it with nagarro id]
1. Link for screen recording video showing everything mentioned above [Access it with nagarro id]
    
    [Screen recording url](https://nagarro-my.sharepoint.com/:v:/p/santosh_goilkar/Ebr6AOyHcsZFhdLCPFUTgpIBxU9nNxGLUDa9-fjLFU2mWA )

**Note - Please watch recording till last. I have merge 2 video for showing service api autoscaling restore back to normal 3 pod at the end. One with muted audio and second with clear audio**

## List of Command Creating image for NodeAPI Project
- I have Created Windows VM on Azure Portal.
- Installed  Docker Desktop on Machine
- Authenticated My Docker Hub account in Docker Desktop
- Copied NodeAPI project in VM.
- Perform  below Command on my NodeAPI folder of project.
- Complete logs for all command are found under "Command Logs" folder

1. Command to List No of container in docker

    `C:\NodeAPI>docker ps`

1. Command to Build the Node API Image with name nodeapi and tag 1

    `C:\NodeAPI>docker build -t nodeapi:1 .`

1. Command to List No of images

    `C:\NodeAPI>docker images`

1. Command to run docker image in container 

    `C:\NodeAPI>docker run --name node-container -p 80:3000 nodeapi:1`

1. Command to login into Docker Hub

    `C:\NodeAPI>docker login`

1. Command to Build the Node API Image with Docker Hub account santoshgoilkar and image name nodeapi and tag 1

    `C:\NodeAPI>docker build -t santoshgoilkar/nodeapi:1 .`

1. Command to push docker image to my Docker Hub account santoshgoilkar  

    `C:\NodeAPI>docker push santoshgoilkar/nodeapi:1`

1. Docker Image URL for NodeAPI project
    NodeAPI Docker Image url
    https://hub.docker.com/r/santoshgoilkar/nodeapi

    Complete logs for all command are found under "Command Logs/CommandLogForNodeApiImageCreation.log" file

## Steps Creating MySQL DB Service

- mysql-config.yaml

Created mysql-config.yaml file under YAML folder of ConfigMap Kind for holding host, user, database setting.

Apply this file from azure portal RUN command window
   
    kubectl apply -f mysql-config.yaml

-  mysql-secret.yaml

Created mysql-secret.yaml file under YAML folder of Secret Kind for holding base64 encoded password.

Apply this file from azure portal RUN command window
   
    kubectl apply -f mysql-secret.yaml  

-  mysql-statefulset.yaml

Created mysql-statefulset.yaml file under YAML folder of StatefulSet Kind for configuring statefulset mysql database with single pod only.
Along with env variable configuration for reading configuration from ConfigMap and Secret And storage access details.

Apply this file from azure portal RUN command window
    
    kubectl apply -f mysql-statefulset.yaml

- mysql-service.yaml

Created mysql-service.yaml file under YAML folder of Service Kind for configuring  custerIP none for service only be accessible from inside the cluster 
with port 3306.

Apply this file from azure portal RUN command window
    
    kubectl apply -f mysql-service.yaml

- mysql-client.yaml

Created mysql-client.yaml file under YAML folder of pod Kind for accessing mysql database service through command line bash from inside private cluster for creating table and inserting data

Apply this file from azure portal RUN command window
    
    kubectl apply -f mysql-client.yaml

## List of command connecting MySQL and creating table and data

1. Command to login into azure portal

    `C:\Program Files\Microsoft SDKs\Azure\.NET SDK\v2.9>az login --use-device-code`

1. Command to connect azure portal kubernetes cluster.

    `C:\Program Files\Microsoft SDKs\Azure\.NET SDK\v2.9>az aks get-credentials --resource-group Kubernet --name KubCluster`

1. Command to connect mysql through bash using mysql-client pod

    `C:\>kubectl exec -it mysql-client -- /bin/bash`

1.  List of command in mysql server for creating table ProductMaster and inserting records 

        bash-5.1# mysql -h mysql -u root -p
        mysql> use mydatabase

        mysql> Create table ProductMaster (
            -> Id INT AUTO_INCREMENT,
            -> NAME VARCHAR(50) NOT NULL,
            -> PRICE INT NOT NULL,
            -> PRIMARY KEY(Id));

        mysql> Insert into ProductMaster Values(1,'Laptop',50000);
        mysql> Insert into ProductMaster Values(2,'Mouse',500);
        mysql> Insert into ProductMaster Values(3,'Keyboard',500);
        mysql> Insert into ProductMaster Values(4,'Mouse Pad',100);
        mysql> Insert into ProductMaster Values(5,'Headphones',1500);
        mysql> select * from ProductMaster; `

    
    Complete logs for all command is found under "Command Logs/CommandLogForCreatingTableAndDataInMySQL.log" file

## Steps for creating Node API Service with 3 pods using deployment

-  api-deployment.yaml

Created api-deployment.yaml file under YAML folder of Deployment Kind for configuring node api service with 3 pods running
using santoshgoilkar/nodeapi:1 image created earlier. 

Along with env variable configuration for reading configuration from ConfigMap and Secret and cpu resources utilization configuration.
  
Apply this file from azure portal RUN command window
    
    kubectl apply -f api-deployment.yaml

-  api-service.yaml

Created api-service.yaml file under YAML folder of Service Kind for configuring node api service with load balancer and
TCP 80 port mapping with target port 3000 

Apply this file from azure portal RUN command window
    
    kubectl apply -f api-service.yaml

-  hpa.yaml

Created hpa.yaml file under YAML folder of HorizontalPodAutoscaler Kind for configuring autoscaling for node api service with 50 % CPU utilization
with minReplicas 3 and maxReplicas 10

Apply this file from azure portal RUN command window
    
    kubectl apply -f hpa.yaml

-  Get the name of one of the API pods
    
        kubectl get pods -l app=api
    
-  Simulate load on one of the pods
    
        kubectl exec -it api-deployment-7484c85968-979fm -- sh -c "while true; do wget -qO- http://localhost:3000; done"



